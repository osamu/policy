# SOME DESCRIPTIVE TITLE.
# Copyright (C) 1996, 1997, 1998 Ian Jackson, Christian Schwarz, 1998-2017,
# The Debian Policy Mailing List
# This file is distributed under the same license as the Debian Policy
# Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Debian Policy Manual 4.1.6.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-08-02 11:56+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.4.0\n"

#: ../../ch-maintainerscripts.rst:2
msgid "Package maintainer scripts and installation procedure"
msgstr ""

#: ../../ch-maintainerscripts.rst:7
msgid "Introduction to package maintainer scripts"
msgstr ""

#: ../../ch-maintainerscripts.rst:9
msgid ""
"It is possible to supply scripts as part of a package which the package "
"management system will run for you when your package is installed, "
"upgraded or removed."
msgstr ""

#: ../../ch-maintainerscripts.rst:13
msgid ""
"These scripts are the control information files ``preinst``, "
"``postinst``, ``prerm`` and ``postrm``. They must be proper executable "
"files; if they are scripts (which is recommended), they must start with "
"the usual ``#!`` convention. They should be readable and executable by "
"anyone, and must not be world-writable."
msgstr ""

#: ../../ch-maintainerscripts.rst:19
msgid ""
"The package management system looks at the exit status from these "
"scripts. It is important that they exit with a non-zero status if there "
"is an error, so that the package management system can stop its "
"processing. For shell scripts this means that you *almost always* need to"
" use ``set -e`` (this is usually true when writing shell scripts, in "
"fact). It is also important, of course, that they exit with a zero status"
" if everything went well."
msgstr ""

#: ../../ch-maintainerscripts.rst:27
msgid ""
"Additionally, packages interacting with users using ``debconf`` in the "
"``postinst`` script should install a ``config`` script as a control "
"information file. See :ref:`s-maintscriptprompt` for details."
msgstr ""

#: ../../ch-maintainerscripts.rst:32
msgid ""
"When a package is upgraded a combination of the scripts from the old and "
"new packages is called during the upgrade procedure. If your scripts are "
"going to be at all complicated you need to be aware of this, and may need"
" to check the arguments to your scripts."
msgstr ""

#: ../../ch-maintainerscripts.rst:37
msgid ""
"Broadly speaking the ``preinst`` is called before (a particular version "
"of) a package is unpacked, and the ``postinst`` afterwards; the ``prerm``"
" before (a version of) a package is removed and the ``postrm`` "
"afterwards."
msgstr ""

#: ../../ch-maintainerscripts.rst:42
msgid ""
"Programs called from maintainer scripts should not normally have a path "
"prepended to them. Before installation is started, the package management"
" system checks to see if the programs ``ldconfig``, ``start-stop-"
"daemon``, and ``update-rc.d`` can be found via the ``PATH`` environment "
"variable. Those programs, and any other program that one would expect to "
"be in the ``PATH``, should thus be invoked without an absolute pathname. "
"Maintainer scripts should also not reset the ``PATH``, though they might "
"choose to modify it by prepending or appending package-specific "
"directories. These considerations really apply to all shell scripts."
msgstr ""

#: ../../ch-maintainerscripts.rst:56
msgid "Maintainer scripts idempotency"
msgstr ""

#: ../../ch-maintainerscripts.rst:58
msgid ""
"It is necessary for the error recovery procedures that the scripts be "
"idempotent. This means that if it is run successfully, and then it is "
"called again, it doesn't bomb out or cause any harm, but just ensures "
"that everything is the way it ought to be. If the first call failed, or "
"aborted half way through for some reason, the second call should merely "
"do the things that were left undone the first time, if any, and exit with"
" a success status if everything is OK.  [#]_"
msgstr ""

#: ../../ch-maintainerscripts.rst:69
msgid "Controlling terminal for maintainer scripts"
msgstr ""

#: ../../ch-maintainerscripts.rst:71
msgid ""
"Maintainer scripts are not guaranteed to run with a controlling terminal "
"and may not be able to interact with the user. They must be able to fall "
"back to noninteractive behavior if no controlling terminal is available. "
"Maintainer scripts that prompt via a program conforming to the Debian "
"Configuration Management Specification (see :ref:`s-maintscriptprompt`) "
"may assume that program will handle falling back to noninteractive "
"behavior."
msgstr ""

#: ../../ch-maintainerscripts.rst:79
msgid ""
"For high-priority prompts without a reasonable default answer, maintainer"
" scripts may abort if there is no controlling terminal. However, this "
"situation should be avoided if at all possible, since it prevents "
"automated or unattended installs. In most cases, users will consider this"
" to be a bug in the package."
msgstr ""

#: ../../ch-maintainerscripts.rst:88
msgid "Exit status"
msgstr ""

#: ../../ch-maintainerscripts.rst:90
msgid ""
"Each script must return a zero exit status for success, or a nonzero one "
"for failure, since the package management system looks for the exit "
"status of these scripts and determines what action to take next based on "
"that datum."
msgstr ""

#: ../../ch-maintainerscripts.rst:98
msgid "Summary of ways maintainer scripts are called"
msgstr ""

#: ../../ch-maintainerscripts.rst:100
msgid ""
"What follows is a summary of all the ways in which maintainer scripts may"
" be called along with what facilities those scripts may rely on being "
"available at that time. Script names preceded by new- are the scripts "
"from the new version of a package being installed, upgraded to, or "
"downgraded to. Script names preceded by old- are the scripts from the old"
" version of a package that is being upgraded from or downgraded from."
msgstr ""

#: ../../ch-maintainerscripts.rst:107
msgid "The ``preinst`` script may be called in the following ways:"
msgstr ""

#: ../../ch-maintainerscripts.rst:109
msgid "``new-preinst`` install"
msgstr ""

#: ../../ch-maintainerscripts.rst:110
msgid "``new-preinst`` install *old-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:111
msgid "``new-preinst`` upgrade *old-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:113
msgid ""
"The package will not yet be unpacked, so the ``preinst`` script cannot "
"rely on any files included in its package. Only essential packages and "
"pre-dependencies (``Pre-Depends``) may be assumed to be available. Pre-"
"dependencies will have been configured at least once, but at the time the"
" ``preinst`` is called they may only be in an \"Unpacked\" or \"Half-"
"Configured\" state if a previous version of the pre-dependency was "
"completely configured and has not been removed since then."
msgstr ""

#: ../../ch-maintainerscripts.rst:129
msgid "``old-preinst`` abort-upgrade *new-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:123
msgid ""
"Called during error handling of an upgrade that failed after unpacking "
"the new package because the ``postrm upgrade`` action failed. The "
"unpacked files may be partly from the new version or partly missing, so "
"the script cannot rely on files included in the package. Package "
"dependencies may not be available. Pre-dependencies will be at least "
"\"Unpacked\" following the same rules as above, except they may be only "
"\"Half-Installed\" if an upgrade of the pre-dependency failed.  [#]_"
msgstr ""

#: ../../ch-maintainerscripts.rst:131
msgid "The ``postinst`` script may be called in the following ways:"
msgstr ""

#: ../../ch-maintainerscripts.rst:138
msgid "``postinst`` configure *most-recently-configured-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:134
msgid ""
"The files contained in the package will be unpacked. All package "
"dependencies will at least be \"Unpacked\". If there are no circular "
"dependencies involved, all package dependencies will be configured. For "
"behavior in the case of circular dependencies, see the discussion in "
":ref:`s-binarydeps`."
msgstr ""

#: ../../ch-maintainerscripts.rst:140
msgid "``old-postinst`` abort-upgrade *new-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:141
msgid "``conflictor's-postinst`` abort-remove in-favour *package* *new-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:142
msgid "``postinst`` abort-remove"
msgstr ""

#: ../../ch-maintainerscripts.rst:143
msgid ""
"``deconfigured's-postinst`` abort-deconfigure in-favour *failed-install-"
"package* *version* [ removing conflicting-package version ]"
msgstr ""

#: ../../ch-maintainerscripts.rst:145
msgid ""
"The files contained in the package will be unpacked. All package "
"dependencies will at least be \"Half-Installed\" and will have previously"
" been configured and not removed. However, dependencies may not be "
"configured or even fully unpacked in some error situations.  [#]_ The "
"``postinst`` should still attempt any actions for which its dependencies "
"are required, since they will normally be available, but consider the "
"correct error handling approach if those actions fail. Aborting the "
"``postinst`` action if commands or facilities from the package "
"dependencies are not available is often the best approach."
msgstr ""

#: ../../ch-maintainerscripts.rst:156
msgid "The ``prerm`` script may be called in the following ways:"
msgstr ""

#: ../../ch-maintainerscripts.rst:158
msgid "``prerm`` remove"
msgstr ""

#: ../../ch-maintainerscripts.rst:159
msgid "``old-prerm`` upgrade *new-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:160
msgid "``conflictor's-prerm`` remove in-favour package *new-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:161
msgid ""
"``deconfigured's-prerm`` deconfigure in-favour *package-being-installed* "
"*version* [removing conflicting-package version]"
msgstr ""

#: ../../ch-maintainerscripts.rst:163
msgid ""
"The package whose ``prerm`` is being called will be at least \"Half-"
"Installed\". All package dependencies will at least be \"Half-Installed\""
" and will have previously been configured and not removed. If there was "
"no error, all dependencies will at least be \"Unpacked\", but these "
"actions may be called in various error states where dependencies are only"
" \"Half-Installed\" due to a partial upgrade."
msgstr ""

#: ../../ch-maintainerscripts.rst:174
msgid "``new-prerm`` failed-upgrade *old-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:172
msgid ""
"Called during error handling when ``prerm upgrade`` fails. The new "
"package will not yet be unpacked, and all the same constraints as for "
"``preinst upgrade`` apply."
msgstr ""

#: ../../ch-maintainerscripts.rst:176
msgid "The ``postrm`` script may be called in the following ways:"
msgstr ""

#: ../../ch-maintainerscripts.rst:178
msgid "``postrm`` remove"
msgstr ""

#: ../../ch-maintainerscripts.rst:179
msgid "``postrm`` purge"
msgstr ""

#: ../../ch-maintainerscripts.rst:180
msgid "``old-postrm`` upgrade *new-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:181
msgid "``disappearer's-postrm`` disappear overwriter *overwriter-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:183
msgid ""
"The ``postrm`` script is called after the package's files have been "
"removed or replaced. The package whose ``postrm`` is being called may "
"have previously been deconfigured and only be \"Unpacked\", at which "
"point subsequent package changes do not consider its dependencies. "
"Therefore, all ``postrm`` actions may only rely on essential packages and"
" must gracefully skip any actions that require the package's dependencies"
" if those dependencies are unavailable. [#]_"
msgstr ""

#: ../../ch-maintainerscripts.rst:197
msgid "``new-postrm`` failed-upgrade *old-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:193
msgid ""
"Called when the old ``postrm upgrade`` action fails. The new package will"
" be unpacked, but only essential packages and pre-dependencies can be "
"relied on. Pre-dependencies will either be configured or will be "
"\"Unpacked\" or \"Half-Configured\" but previously had been configured "
"and was never removed."
msgstr ""

#: ../../ch-maintainerscripts.rst:199
msgid "``new-postrm`` abort-install"
msgstr ""

#: ../../ch-maintainerscripts.rst:200
msgid "``new-postrm`` abort-install *old-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:201
msgid "``new-postrm`` abort-upgrade *old-version*"
msgstr ""

#: ../../ch-maintainerscripts.rst:203
msgid ""
"Called before unpacking the new package as part of the error handling of "
"``preinst`` failures. May assume the same state as ``preinst`` can "
"assume."
msgstr ""

#: ../../ch-maintainerscripts.rst:210
msgid "Details of unpack phase of installation or upgrade"
msgstr ""

#: ../../ch-maintainerscripts.rst:212
msgid ""
"The procedure on installation/upgrade/overwrite/disappear (i.e., when "
"running ``dpkg --unpack``, or the unpack stage of ``dpkg --install``) is "
"as follows.  [#]_ In each case, if a major error occurs (unless listed "
"below) the actions are, in general, run backwards - this means that the "
"maintainer scripts are run with different arguments in reverse order. "
"These are the \"error unwind\" calls listed below."
msgstr ""

#: ../../ch-maintainerscripts.rst:219
msgid "Notify the currently installed package:"
msgstr ""

#: ../../ch-maintainerscripts.rst:221
msgid "If a version of the package is already \"Installed\", call"
msgstr ""

#: ../../ch-maintainerscripts.rst:227
msgid ""
"If the script runs but exits with a non-zero exit status, ``dpkg`` will "
"attempt:"
msgstr ""

#: ../../ch-maintainerscripts.rst:234
msgid ""
"If this works, the upgrade continues. If this does not work, the error "
"unwind:"
msgstr ""

#: ../../ch-maintainerscripts.rst:241
msgid ""
"If this works, then the *old-version* is \"Installed\", if not, the old "
"version is in a \"Half-Configured\" state."
msgstr ""

#: ../../ch-maintainerscripts.rst:244
msgid ""
"If a \"conflicting\" package is being removed at the same time, or if any"
" package will be broken (due to ``Breaks``):"
msgstr ""

#: ../../ch-maintainerscripts.rst:247
msgid ""
"If ``--auto-deconfigure`` is specified, call, for each package to be "
"deconfigured due to ``Breaks``:"
msgstr ""

#: ../../ch-maintainerscripts.rst:255 ../../ch-maintainerscripts.rst:276
#: ../../ch-maintainerscripts.rst:295 ../../ch-maintainerscripts.rst:336
#: ../../ch-maintainerscripts.rst:352
msgid "Error unwind:"
msgstr ""

#: ../../ch-maintainerscripts.rst:262 ../../ch-maintainerscripts.rst:284
msgid ""
"The deconfigured packages are marked as requiring configuration, so that "
"if ``--install`` is used they will be configured again if possible."
msgstr ""

#: ../../ch-maintainerscripts.rst:266
msgid ""
"If any packages depended on a conflicting package being removed and "
"``--auto-deconfigure`` is specified, call, for each such package:"
msgstr ""

#: ../../ch-maintainerscripts.rst:288
msgid "To prepare for removal of each conflicting package, call:"
msgstr ""

#: ../../ch-maintainerscripts.rst:302
msgid "Run the ``preinst`` of the new package:"
msgstr ""

#: ../../ch-maintainerscripts.rst:304
msgid "If the package is being upgraded, call:"
msgstr ""

#: ../../ch-maintainerscripts.rst:310
msgid "If this fails, we call:"
msgstr ""

#: ../../ch-maintainerscripts.rst:316
msgid "If that works, then"
msgstr ""

#: ../../ch-maintainerscripts.rst:322
msgid ""
"is called. If this works, then the old version is in an \"Installed\" "
"state, or else it is left in an \"Unpacked\" state."
msgstr ""

#: ../../ch-maintainerscripts.rst:325
msgid "If it fails, then the old version is left in an \"Half-Installed\" state."
msgstr ""

#: ../../ch-maintainerscripts.rst:328
msgid ""
"Otherwise, if the package had some configuration files from a previous "
"version installed (i.e., it is in the \"Config-Files\" state):"
msgstr ""

#: ../../ch-maintainerscripts.rst:342
msgid ""
"If this fails, the package is left in a \"Half-Installed\" state, which "
"requires a reinstall. If it works, the packages is left in a \"Config-"
"Files\" state."
msgstr ""

#: ../../ch-maintainerscripts.rst:346
msgid "Otherwise (i.e., the package was completely purged):"
msgstr ""

#: ../../ch-maintainerscripts.rst:358
msgid ""
"If the error-unwind fails, the package is in a \"Half-Installed\" phase, "
"and requires a reinstall. If the error unwind works, the package is in "
"the \"Not-Installed\" state."
msgstr ""

#: ../../ch-maintainerscripts.rst:362
msgid ""
"The new package's files are unpacked, overwriting any that may be on the "
"system already, for example any from the old version of the same package "
"or from another package. Backups of the old files are kept temporarily, "
"and if anything goes wrong the package management system will attempt to "
"put them back as part of the error unwind."
msgstr ""

#: ../../ch-maintainerscripts.rst:368
msgid ""
"It is an error for a package to contain files which are on the system in "
"another package, unless ``Replaces`` is used (see :ref:`s-replaces`)."
msgstr ""

#: ../../ch-maintainerscripts.rst:372
msgid ""
"It is a more serious error for a package to contain a plain file or other"
" kind of non-directory where another package has a directory (again, "
"unless ``Replaces`` is used). This error can be overridden if desired "
"using ``--force-overwrite-dir``, but this is not advisable."
msgstr ""

#: ../../ch-maintainerscripts.rst:378
msgid ""
"Packages which overwrite each other's files produce behavior which, "
"though deterministic, is hard for the system administrator to understand."
" It can easily lead to \"missing\" programs if, for example, a package is"
" unpacked which overwrites a file from another package, and is then "
"removed again.  [#]_"
msgstr ""

#: ../../ch-maintainerscripts.rst:384
msgid ""
"A directory will never be replaced by a symbolic link to a directory or "
"vice versa; instead, the existing state (symlink or not) will be left "
"alone and ``dpkg`` will follow the symlink if there is one."
msgstr ""

#: ../../ch-maintainerscripts.rst:388
msgid "If the package is being upgraded:"
msgstr ""

#: ../../ch-maintainerscripts.rst:390
msgid "Call:"
msgstr ""

#: ../../ch-maintainerscripts.rst:396
msgid "If this fails, ``dpkg`` will attempt:"
msgstr ""

#: ../../ch-maintainerscripts.rst:402
msgid "If this works, installation continues. If not, Error unwind:"
msgstr ""

#: ../../ch-maintainerscripts.rst:408 ../../ch-maintainerscripts.rst:415
msgid ""
"If this fails, the old version is left in a \"Half-Installed\" state. If "
"it works, dpkg now calls:"
msgstr ""

#: ../../ch-maintainerscripts.rst:422
msgid "If this fails, the old version is in an \"Unpacked\" state."
msgstr ""

#: ../../ch-maintainerscripts.rst:424
msgid ""
"This is the point of no return. If ``dpkg`` gets this far, it won't back "
"off past this point if an error occurs. This will leave the package in a "
"fairly bad state, which will require a successful re-installation to "
"clear up, but it's when ``dpkg`` starts doing things that are "
"irreversible."
msgstr ""

#: ../../ch-maintainerscripts.rst:430
msgid ""
"Any files which were in the old version of the package but not in the new"
" are removed."
msgstr ""

#: ../../ch-maintainerscripts.rst:433
msgid "The new file list replaces the old."
msgstr ""

#: ../../ch-maintainerscripts.rst:435
msgid "The new maintainer scripts replace the old."
msgstr ""

#: ../../ch-maintainerscripts.rst:437
msgid ""
"Any packages all of whose files have been overwritten during the "
"installation, and which aren't required for dependencies, are considered "
"to have been removed. For each such package"
msgstr ""

#: ../../ch-maintainerscripts.rst:441
msgid "``dpkg`` calls:"
msgstr ""

#: ../../ch-maintainerscripts.rst:448
msgid "The package's maintainer scripts are removed."
msgstr ""

#: ../../ch-maintainerscripts.rst:450
msgid ""
"It is noted in the status database as being in a sane state, namely "
"\"Not-Installed\" (any conffiles it may have are ignored, rather than "
"being removed by ``dpkg``). Note that disappearing packages do not have "
"their prerm called, because ``dpkg`` doesn't know in advance that the "
"package is going to vanish."
msgstr ""

#: ../../ch-maintainerscripts.rst:456
msgid ""
"Any files in the package we're unpacking that are also listed in the file"
" lists of other packages are removed from those lists. (This will "
"lobotomize the file list of the \"conflicting\" package if there is one.)"
msgstr ""

#: ../../ch-maintainerscripts.rst:461
msgid "The backup files made during installation, above, are deleted."
msgstr ""

#: ../../ch-maintainerscripts.rst:463
msgid "The new package's status is now sane, and recorded as \"Unpacked\"."
msgstr ""

#: ../../ch-maintainerscripts.rst:465
msgid ""
"Here is another point of no return: if the conflicting package's removal "
"fails we do not unwind the rest of the installation. The conflicting "
"package is left in a half-removed limbo."
msgstr ""

#: ../../ch-maintainerscripts.rst:469
msgid ""
"If there was a conflicting package we go and do the removal actions "
"(described below), starting with the removal of the conflicting package's"
" files (any that are also in the package being unpacked have already been"
" removed from the conflicting package's file list, and so do not get "
"removed now)."
msgstr ""

#: ../../ch-maintainerscripts.rst:478
msgid "Details of configuration"
msgstr ""

#: ../../ch-maintainerscripts.rst:480
msgid ""
"When we configure a package (this happens with ``dpkg --install`` and "
"``dpkg --configure``), we first update any ``conffile``\\ s and then "
"call:"
msgstr ""

#: ../../ch-maintainerscripts.rst:487
msgid ""
"No attempt is made to unwind after errors during configuration. If the "
"configuration fails, the package is in a \"Half-Configured\" state, and "
"an error message is generated."
msgstr ""

#: ../../ch-maintainerscripts.rst:491
msgid ""
"If there is no most recently configured version ``dpkg`` will pass a null"
" argument.  [#]_"
msgstr ""

#: ../../ch-maintainerscripts.rst:497
msgid "Details of removal and/or configuration purging"
msgstr ""

#: ../../ch-maintainerscripts.rst:503
msgid "If prerm fails during replacement due to conflict"
msgstr ""

#: ../../ch-maintainerscripts.rst:510
msgid "Or else we call:"
msgstr ""

#: ../../ch-maintainerscripts.rst:516
msgid ""
"If this fails, the package is in a \"Half-Configured\" state, or else it "
"remains \"Installed\"."
msgstr ""

#: ../../ch-maintainerscripts.rst:519
msgid "The package's files are removed (except ``conffile``\\ s)."
msgstr ""

#: ../../ch-maintainerscripts.rst:525
msgid ""
"If it fails, there's no error unwind, and the package is in an \"Half-"
"Installed\" state."
msgstr ""

#: ../../ch-maintainerscripts.rst:528
msgid "All the maintainer scripts except the ``postrm`` are removed."
msgstr ""

#: ../../ch-maintainerscripts.rst:530
msgid ""
"If we aren't purging the package we stop here. Note that packages which "
"have no ``postrm`` and no ``conffile``\\ s are automatically purged when "
"removed, as there is no difference except for the ``dpkg`` status."
msgstr ""

#: ../../ch-maintainerscripts.rst:535
msgid ""
"The ``conffile``\\ s and any backup files (``~``-files, ``#*#`` files, "
"``%``-files, ``.dpkg-{old,new,tmp}``, etc.) are removed."
msgstr ""

#: ../../ch-maintainerscripts.rst:542
msgid "If this fails, the package remains in a \"Config-Files\" state."
msgstr ""

#: ../../ch-maintainerscripts.rst:544
msgid "The package's file list is removed."
msgstr ""

#: ../../ch-maintainerscripts.rst:547
msgid ""
"This is so that if an error occurs, the user interrupts ``dpkg`` or some "
"other unforeseen circumstance happens you don't leave the user with a "
"badly-broken package when ``dpkg`` attempts to repeat the action."
msgstr ""

#: ../../ch-maintainerscripts.rst:553
msgid ""
"This can happen if the new version of the package no longer pre-depends "
"on a package that had been partially upgraded."
msgstr ""

#: ../../ch-maintainerscripts.rst:557
msgid ""
"For example, suppose packages foo and bar are \"Installed\" with foo "
"depending on bar. If an upgrade of bar were started and then aborted, and"
" then an attempt to remove foo failed because its ``prerm`` script "
"failed, foo's ``postinst abort-remove`` would be called with bar only "
"\"Half-Installed\"."
msgstr ""

#: ../../ch-maintainerscripts.rst:564
msgid ""
"This is often done by checking whether the command or facility the "
"``postrm`` intends to call is available before calling it. For example:"
msgstr ""

#: ../../ch-maintainerscripts.rst:574
msgid ""
"in ``postrm`` purges the ``debconf`` configuration for the package if "
"debconf is installed."
msgstr ""

#: ../../ch-maintainerscripts.rst:578
msgid ""
"See :doc:`ap-flowcharts` for flowcharts illustrating the processes "
"described here."
msgstr ""

#: ../../ch-maintainerscripts.rst:582
msgid "Part of the problem is due to what is arguably a bug in ``dpkg``."
msgstr ""

#: ../../ch-maintainerscripts.rst:585
msgid ""
"Historical note: Truly ancient (pre-1997) versions of ``dpkg`` passed "
"``<unknown>`` (including the angle brackets) in this case. Even older "
"ones did not pass a second argument at all, under any circumstance. Note "
"that upgrades using such an old dpkg version are unlikely to work for "
"other reasons, even if this old argument behavior is handled by your "
"postinst script."
msgstr ""

